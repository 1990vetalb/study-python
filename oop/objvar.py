#!/usr/bin/env python3

class Robot:

    # Переменная класса, содержащая количество роботов

    # метод howMany і свойство population не відноситься до самого обекта він віддноситься до класу тобто в кожном обекті він буде як 1
    # і всі дані будуть в усих обектах одинакові і якщо якийсь обект зміне це то зміниться і в інших
    population = 0

    def __init__(self, name):
        '''Инициализация данных.'''
        self.name = name
        print('(Инициализация {0})'.format(self.name))
        # При создании этой личности, робот добавляется
        # к переменной 'population'
        Robot.population += 1
    def __del__(self):
        print('{0} уничтожается!'.format(self.name))
        Robot.population -= 1
        if Robot.population == 0:
            print('{0} был последним.'.format(self.name))
        else:
            print('Осталось {0:d} работающих роботов.'.format(Robot.population))
    def sayHi(self):
        print('Приветствую! Мои хозяева называют меня {0}.'.format(self.name))

    @staticmethod # указуєм що це статичний метод
    def howMany():
        '''Выводит численность роботов.'''
        print('У нас {0:d} роботов.'.format(Robot.population))

droid1 = Robot('R2-D2')
droid3 = Robot('R2-D4')
droid2 = Robot('C-3PO')

droid2.sayHi()
Robot.howMany()

print("\nЗдесь роботы могут проделать какую-то работу.\n")
print("Роботы закончили свою работу. Давайте уничтожим их.")

del droid1
del droid2
del droid3

Robot.howMany()



























