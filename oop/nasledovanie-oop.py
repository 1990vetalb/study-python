#!/usr/bin/env python3

#Базовый клас для студента и учителя который имеет общие свойства и методы
class SchoolMember:

    def __init__(self, name, age):
        self.name = name
        self.age = age
        print('(Создан SchoolMember: {0})'.format(self.name))

    def tell(self):
        print('Имя:"{0}" Возраст:"{1}"'.format(self.name, self.age), end=" ")

# Клас учителя
class Teacher(SchoolMember):

    def __init__(self, name, age, salary):
        SchoolMember.__init__(self, name, age)
        self.salary = salary
        print('(Создан Teacher: {0})'.format(self.name))

    def tell(self):
        SchoolMember.tell(self)
        print('Зарплата: "{0:d}"'.format(self.salary))

# Клас студента
class Student(SchoolMember):

    def __init__(self, name, age, marks):
        SchoolMember.__init__(self, name, age)
        self.marks = marks
        print('(Создан Student: {0})'.format(self.name))

    def tell(self):
        SchoolMember.tell(self)
        print('Оценки: "{0:d}"'.format(self.marks))



t = Teacher('Mrs. Shrividya', 40, 30000)
s = Student('Swaroop', 25, 75)
print()  # печатает пустую строку
members = [t, s]
for member in members:
    member.tell()  # работает как для преподавателя, так и для студента


#так же есть абстрактые класы как и в пхп которые можно только наслдовать и нельзя создавать их екземпляр
class SchoolMember(metaclass=ABCMeta):





















