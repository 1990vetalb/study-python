#!/usr/bin/env python3

# модули в питоне

import sys

# так можна подключать свои файлы и далее их использовать
import moduletest

moduletest.testFunctionInModule('text')

print('Аргументы командной строки:')
for i in sys.argv:
    print(i)

print('\n\nПеременная PYTHONPATH содержит', sys.path, '\n')

# посмотрть какие переменіе есть в модуле
print(dir(sys))

print(dir())
a = 5
print(dir())
del a
print(dir())































