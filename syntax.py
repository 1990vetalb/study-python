#!/usr/bin/env python3

# Math
a = 10
b = 2
ab = a + b  # Додавання
print(ab)
ab = a - b  # Віднімання
print(ab)
ab = a * b  # Множення
print(ab)
ab = a / b  # Ділення
print(ab)
ab = 13 // 2  # Ділення без остатку (тобто скільки чисел вміститься в другому числу в числі 13 двійка(2) вміститься 6 рас як і в числі 12)
print(ab)
ab = a ** b  # Возведення в ступінь тобто число "а" буде возведенно в ступінь "b"
print(ab)
ab = a % b  # остаток от деления
print(ab)
a /= 2  # Ділим пєрємєну на 2 і сразуж перезаписуєм значеня її
print(a)
a *= 2  # Множим пєрємєну на 2 і сразуж перезаписуєм значеня її
print(a)
f = 'Text'
f *= 5 #Дублюєм текст 5 раз
print(f)


# Input
# команда для консольного ввода. що користувач введе те  буде в пєрємєній "inputValue"
# inputValue1 = int(input('Введите число 1: '))
# inputValue2 = int(input('Введите число 2: '))
# print(inputValue1 + inputValue2)


# del inputValue1  # Видалення пєрємєнной


# IF ELIF ELSE
val1 = 2
val2 = 12
if val1 < val2:
    print('{} < {}'.format(val1, val2))
elif val1 == val2:
    print('{} = {}'.format(val1, val2))
else:
    print('{} > {}'.format(val1, val2))


# Типи данних
int('2')
str(2)
float(2)


# Cycles
i = 0
while i < 10:
    print(i)
    i += 1


for j in 'qwerty':
    if j == 'r':
        continue
    print(j, end='')


for j in [1, 2, 3, 4]:
    if j == 3:
        break
    print(j)
else:
    print('break')


for j in [1, 2, 3, 4]:
    if j == 5:
        break
    print(j)
else:
    print('break didnt use')


# List

listCustom = [1, 56, 'x', [1, 2, 'w'], 56, 'haha']
print(listCustom)

listCustom.append(123)  # добавление в список новое значение в конец
print(listCustom)

listB = [222, 333]
listCustom.extend(listB)  # добавление в список все значния другого списка в конец
print(listCustom)

listB = [222, 333]
listCustom.insert(1, 'ssssss')  # добавление в список значния по ключу но елемент с ключом не удаляется а смещается и все елементы смещаються
print(listCustom)

listCustom.remove('ssssss')  # Удаляет елемент по значнию
print(listCustom)

listCustom.pop(2)  # Удаляет елемент по индексу
print(listCustom)

print(listCustom.index(56))  # Возвращает индекс елемента

print(listCustom.count(56))  # Возвращает количество елементов в списке

listA = ['2', '4']
listA.sort()  # сортирует список со значениями по одинаковому типу тоесть строка и число не будет сортироватся
print(listA)

listCustom.reverse()  # переварачивает список
print(listCustom)

listCustom.clear()  # Очищает весь список
print(listCustom)

listG = [4, 55, 'w', 33.44]
print(listG[0])
print(listG[-1])

print(listG[0:2])  # Вывод с 0 индекса по 2 не включительно

#  list(START:STOP:STEP) указывает с какого индекта начинать до какого продолжнать и с камим шагом это делать
print(listG[::2])  # Вывод каждого второго елемента






