#!/usr/bin/env python3

def sayHello():
    print('Привет, Мир!') # блок, принадлежащий функции
    # Конец функции

sayHello() # вызов функции
sayHello() # ещё один вызов функции



def printMax(a, b):
    if a > b:
        print(a, 'максимально')
    elif a == b:
        print(a, 'равно', b)
    else:
        print(b, 'максимально')

printMax(3, 4) # прямая передача значений

x = 5
y = 7
printMax(x, y) # передача переменных в качестве аргументов

#Не локалные переменные
def func_outer():
    x = 2
    print('x равно', x)

    def func_inner():
        nonlocal x
        x = 5

    func_inner()
    print('Локальное x сменилось на', x)

func_outer()

# nonlocal - с помощью цього атрибуту ми можем достукатись до переменно в родітєльскій функції
def funcTest():
    test = 5

    def test2():
        nonlocal test
        test = 2

    test2()
    print(test)

funcTest()

# так можна конкретно указать якому параметру в функції ми передаєм значення а не просто як в ПХП тільки по очереді
def func(a, b = 5, c = 10):
    print('a равно', a, ', b равно', b, ', а c равно', c)

func(3, 7)
func(25, c = 24)
func(c = 50, a = 100)


# таким образом можем указувати бескінечне колічество вводних чисел або перемених з числами головне не нарушать очерьодность
def total(initial=5, *numbers, **keywords):
    print(initial, numbers, keywords) # 10 (1, 2, 3) {'ss': 22, 'aa': 22} initial = 10 numbers =(1, 2, 3) keywords = {'ss': 22, 'aa': 22}

    count = initial
    for number in numbers:
        count += number
    for key in keywords:
        count += keywords[key]
    return count

print(total(10, 1, 2, 3, ss = 22, aa = 22))

# функція просто нічого не верта так можна указать через атрибут пасс
def someFunction():
    pass

print(someFunction())

#так можна документувати функціїї і также визивати через функцію прочитати інфу про цю функцію  printMax.__doc__
def printMax(x, y):
    '''Выводит максимальное из двух чисел.

    Оба значения должны быть целыми числами.'''
    x = int(x) # конвертируем в целые, если возможно
    y = int(y)
    if x > y:
         print(x, 'наибольшее')
    else:
         print(y, 'наибольшее')

printMax(3, 5)
print(printMax.__doc__)





