#!/usr/bin/env python3

number = 23
running = False #встановити true щоб цикл працював

while running:
    guess = int(input('Введите целое число : '))
    if guess == number:
        print('Поздравляю, вы угадали.')
        running = False # это останавливает цикл while
    elif guess < number:
        print('Нет, загаданное число немного больше этого')
    else:
        print('Нет, загаданное число немного меньше этого.')
else:
    print('Цикл while закончен.')
    # Здесь можете выполнить всё что вам ещё нужно

print('Завершение.')




#для перебора масіва
for i in range(1, 5):
    print(i)
else:
    print('Цикл for закончен')




#Цикл з условієм буде виконуватись до тих пор пока не збудеться условіє і виконаеться брейк
#встановити True щоб цикл працював
while False:
    s = input('Введите что-нибудь : ')
    if s == 'выход':
        break
    print('Длина строки: ', len(s))
print('Завершение')


#встановити True щоб цикл працював
while False:
    s = input('Введите что-нибудь : ')
    if s == 'выход':
        break
    if len(s) < 3:
        print('Слишком мало')
        continue
    print('Введённая строка достаточной дkbys')



















